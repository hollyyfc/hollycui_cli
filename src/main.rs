use serde::Deserialize;
use std::env;
use std::fs::File;
use std::process;
use csv::ReaderBuilder;

#[derive(Debug, Deserialize, PartialEq, Clone)]
#[allow(dead_code)] // This line suppresses the warning about unused fields
struct Book {
    name: String,
    author: String,
    genre: String,
    gross_sales: f64, 
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() < 3 {
        eprintln!("Usage: {} <field> <value>", args[0]);
        process::exit(1);
    }

    let field = &args[1];
    let value = &args[2];

    match read_books_from_csv("books.csv") {
        Ok(books) => {
            let filtered_books = filter_books_by_name_or_author(&books, field, value);
            if filtered_books.is_empty() {
                println!("No books found with {} = {}", field, value);
            } else {
                for book in filtered_books {
                    println!("{:?}", book);
                }
            }
        },
        Err(e) => {
            eprintln!("Error reading books: {}", e);
            process::exit(1);
        },
    }
}

fn read_books_from_csv(file_path: &str) -> Result<Vec<Book>, csv::Error> {
    let file = File::open(file_path)?;
    let mut rdr = ReaderBuilder::new().from_reader(file);
    rdr.deserialize().collect()
}

fn filter_books_by_name_or_author(books: &[Book], field: &str, value: &str) -> Vec<Book> {
    books.iter().filter(|book| {
        match field {
            "name" => book.name == value,
            "author" => book.author == value,
            _ => false,
        }
    }).cloned().collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_filter_books_by_name() {
        let books = vec![
            Book { name: "Beowulf".to_string(), author: "Unknown, Seamus Heaney".to_string(), genre: "fiction".to_string(), gross_sales: 34160.0 },
            Book { name: "Red Queen".to_string(), author: "Victoria Aveyard".to_string(), genre: "fiction".to_string(), gross_sales: 19960.0 },
        ];

        let filtered_books = filter_books_by_name_or_author(&books, "name", "Beowulf");
        assert_eq!(filtered_books.len(), 1);
        assert_eq!(filtered_books[0].name, "Beowulf");
    }

    #[test]
    fn test_filter_books_by_author() {
        let books = vec![
            Book { name: "Beowulf".to_string(), author: "Unknown, Seamus Heaney".to_string(), genre: "fiction".to_string(), gross_sales: 34160.0 },
            Book { name: "Red Queen".to_string(), author: "Victoria Aveyard".to_string(), genre: "fiction".to_string(), gross_sales: 19960.0 },
        ];

        let filtered_books = filter_books_by_name_or_author(&books, "author", "Victoria Aveyard");
        assert_eq!(filtered_books.len(), 1);
        assert_eq!(filtered_books[0].author, "Victoria Aveyard");
    }

    #[test]
    fn test_filter_books_by_author2() {
        let books = vec![
            Book { name: "The Forgotten Garden".to_string(), author: "Kate Morton".to_string(), genre: "fiction".to_string(), gross_sales: 4907.7 },
            Book { name: "A Little Princess".to_string(), author: "Frances Hodgson Burnett, Nancy Bond".to_string(), genre: "fiction".to_string(), gross_sales: 23792.34 },
        ];

        let filtered_books = filter_books_by_name_or_author(&books, "author", "Frances Hodgson Burnett, Nancy Bond");
        assert_eq!(filtered_books.len(), 1);
        assert_eq!(filtered_books[0].author, "Frances Hodgson Burnett, Nancy Bond");
    }

    #[test]
    fn test_filter_books_by_name2() {
        let books = vec![
            Book { name: "The Forgotten Garden".to_string(), author: "Kate Morton".to_string(), genre: "fiction".to_string(), gross_sales: 4907.7 },
            Book { name: "A Little Princess".to_string(), author: "Frances Hodgson Burnett, Nancy Bond".to_string(), genre: "fiction".to_string(), gross_sales: 23792.34 },
        ];

        let filtered_books = filter_books_by_name_or_author(&books, "name", "The Forgotten Garden");
        assert_eq!(filtered_books.len(), 1);
        assert_eq!(filtered_books[0].name, "The Forgotten Garden");
    }
}
