[![pipeline status](https://gitlab.com/hollyyfc/hollycui_cli/badges/main/pipeline.svg)](https://gitlab.com/hollyyfc/hollycui_cli/-/commits/main)



# 🪛 Holly's Rust Command-Line Tool with Testing

## Description

This project integrates the versatile [Rust command-line tool](https://rust-cli.github.io/book/index.html) designed for efficiently filtering and processing book data from CSV files. It functions as an improved extension step of my [previous project](https://gitlab.com/hollyyfc/hollycui_vector) on Vector database. It leverages Rust's capabilities that perform data ingestion, filtering based on user-specified criteria (i.e., book name or author name), and outputting relevant information, to provide a seamless integration between the command-line interface and the core logic of the application. Moreover, [unit testings](https://doc.rust-lang.org/cargo/commands/cargo-test.html) have been created and enabled to ensure the reliability and correstness of the output. 

## Demo

The sample tests are conducted on the data called `books.csv` (retrieved from dataset [Books Sales and Ratings](https://www.kaggle.com/datasets/thedevastator/books-sales-and-ratings) on Kaggle and subsequently cleaned by me for project use). The Rust functions are designed to filter and query some relevant information of books (`name`, `author`, `genre`, and `gross_sales`) based on the users' input. 

- **Command-Line Tool with Book Name**

If the users wish to query a book's info by the name of the book, they could supply `name` as the variable that the query will match on, and then specify `"<book_name>"`. The command-line tool should be called in the format of: `./target/debug/mini8 name "<book_name>"`. Since book names are unique, there should be only one data entry returned by the function:

![cli_name](https://gitlab.com/hollyyfc/hollycui_cli/-/wikis/uploads/094b3f411020fd2cd0210b638faf16e5/cli_name.png)

- **Command-Line Tool with Author Name**

If the users wish to query all books' info by the name of an author, they could supply `author` as the variable that the query will match on, and then specify `"<author_name>"`. The command-line tool should be called in the format of: `./target/debug/mini8 author "<author_name>"`. Since an author could have more than one best-selling books in the dataset, there should be at least one data entry returned by the function:

![cli_author](https://gitlab.com/hollyyfc/hollycui_cli/-/wikis/uploads/cbdef2d1b1b371cb823464be584155ee/cli_author.png)

- **Command-Line Tool with Unknown Input**

If the book or the author that the users wish to find was not incorporated in the dataset, the command-line tool simply returns *No books found*:

![unknown](https://gitlab.com/hollyyfc/hollycui_cli/-/wikis/uploads/77bff21a4b7c10ccc5fcf19e043c504a/unknown.png)

- **Cargo Test**

Unit tests are integrated within `src/main.rs`. Calling `cargo test` will yield the following output:

![test](https://gitlab.com/hollyyfc/hollycui_cli/-/wikis/uploads/011f6558f0a0788c771dedc15d25fd9c/test.png)

The same testing report has also been generated to a text file using `cargo test 2>&1 | tee ./test_report.txt`. The file is saved as `test_report.txt` in the repo. 

## Steps Walkthrough

- `cargo new <PROJECT-NAME>` in desired directory
- Add `csv` and `serde` to `Cargo.toml`
- Build Rust functions in `src/main.rs`
    - Define the book structure
    - Write functions for reading CSV files and filtering books by name or author
    - Implement command-line tool structures:
        ```rust
        let args: Vec<String> = env::args().collect();
        if args.len() < 3 {
            eprintln!("Usage: {} <field> <value>", args[0]);
            process::exit(1);
        }
        let field = &args[1];
        let value = &args[2];
        ```
    - Finish `fn main()` with book matching
    - Add unit tests:
        ```rust
        #[cfg(test)]
        mod tests {
            use super::*;

            #[test]
            fn test_filter_books_by_name() {
                let books = vec![
                    Book { 
                        name: "Beowulf".to_string(), 
                        author: "Unknown, Seamus Heaney".to_string(), 
                        genre: "fiction".to_string(), 
                        gross_sales: 34160.0 
                        },
                    Book { 
                        name: "Red Queen".to_string(), 
                        author: "Victoria Aveyard".to_string(), 
                        genre: "fiction".to_string(), 
                        gross_sales: 19960.0 
                        },
                ];
                let filtered_books = filter_books_by_name_or_author(&books, "name", "Beowulf");
                assert_eq!(filtered_books.len(), 1);
                assert_eq!(filtered_books[0].name, "Beowulf");
            }

            // Add more test cases ...
        }
        ```
- `cargo build`
- Utilize command-line tool in the format defined above
- `cargo test`